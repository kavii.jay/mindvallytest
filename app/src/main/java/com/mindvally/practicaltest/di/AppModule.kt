package com.mindvally.practicaltest.di

import android.app.Application
import com.mindvally.practicaltest.apiclient.apis.ApiService
import com.mindvally.practicaltest.app.data.AppDao
import com.mindvally.practicaltest.app.data.AppDatabase
import com.mindvally.practicaltest.core.general.Common
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun getRetroAPIserviceInstance(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    private val okHttpClient: okhttp3.OkHttpClient = okhttp3.OkHttpClient.Builder()
        .readTimeout((60 * 2).toLong(), TimeUnit.SECONDS)
        .connectTimeout((60 * 2).toLong(), TimeUnit.SECONDS)
        .writeTimeout((60 * 2).toLong(), TimeUnit.SECONDS)
        .build()


    @Singleton
    @Provides
    fun setRetroInstance(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(Common.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
    }


    @Singleton
    @Provides
    fun getAppDB(application: Application): AppDatabase {
        return AppDatabase.getAppDb(application)
    }

    @Singleton
    @Provides
    fun getDao(appdb: AppDatabase): AppDao {
        return appdb.GetAppDao()
    }

}