package com.mindvally.practicaltest.app.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.mindvally.practicaltest.R
import com.mindvally.practicaltest.apiclient.apisupport.response.channel.LatestMedia

class CourseAdapter
    (
    private val latestMedia: List<LatestMedia>,
    private val context: Context
) :
    RecyclerView.Adapter<CourseAdapter.EpisodeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EpisodeViewHolder {
        return EpisodeViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.new_episode_recycle_view, parent, false)
        )
    }

    override fun onBindViewHolder(holder: EpisodeViewHolder, position: Int) = holder.run {
        onBind(position)
        val animation = AnimationUtils.loadAnimation(holder.itemView.context, R.anim.left_to_rigth)
        holder.itemView.startAnimation(animation)
    }

    override fun getItemCount(): Int {

        return if (latestMedia.isEmpty()) {
            0
        } else {
            6
        }
    }


    inner class EpisodeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val coverImage = itemView.findViewById<ImageView>(R.id.episodeBookImage)
        private val epiTitle = itemView.findViewById<TextView>(R.id.epiTitle)
        private val subTitle = itemView.findViewById<TextView>(R.id.subTitle)
        fun onBind(position: Int) {
            subTitle.visibility = View.GONE
            Glide.with(context).load(latestMedia[position].coverAsset.url)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.placeholder)
                .into(coverImage)
            epiTitle.text = latestMedia[position].title

        }

    }
}