package com.mindvally.practicaltest.app.presentation.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.mindvally.practicaltest.R
import com.mindvally.practicaltest.apiclient.apisupport.response.newepisode.Media

class NewEpisodeAdapter
    (
    private val newEpisode: List<Media>,
    private val context: Context
) :
    RecyclerView.Adapter<NewEpisodeAdapter.EpisodeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EpisodeViewHolder {
        return EpisodeViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.new_episode_recycle_view, parent, false)
        )
    }

    override fun onBindViewHolder(holder: EpisodeViewHolder, position: Int) = holder.run {
        onBind(position)
        val animation = AnimationUtils.loadAnimation(holder.itemView.context, R.anim.rigt_to_left)
        holder.itemView.startAnimation(animation)
    }

    override fun getItemCount(): Int {

        return if (newEpisode.isEmpty()) {
            0
        } else {
            6
        }
    }


    inner class EpisodeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val coverImage = itemView.findViewById<ImageView>(R.id.episodeBookImage)
        private val epiTitle = itemView.findViewById<TextView>(R.id.epiTitle)
        private val subTitle = itemView.findViewById<TextView>(R.id.subTitle)
        fun onBind(position: Int) {

            Glide.with(context).load(newEpisode[position].coverAsset.url)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.placeholder)
                .into(coverImage)
            epiTitle.text = newEpisode[position].title
            subTitle.text = newEpisode[position].channel.title
            Log.i("printData", "urls :" + newEpisode[position].coverAsset.url)
        }

    }
}