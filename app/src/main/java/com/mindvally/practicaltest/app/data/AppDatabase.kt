package com.mindvally.practicaltest.app.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.mindvally.practicaltest.apiclient.apisupport.response.category.CategoryResponse
import com.mindvally.practicaltest.apiclient.apisupport.response.channel.ChannelResponse
import com.mindvally.practicaltest.apiclient.apisupport.response.newepisode.NewEpisodesResponse


@Database(entities = [NewEpisodesResponse::class, ChannelResponse::class, CategoryResponse::class], version = 1, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {

    abstract fun GetAppDao(): AppDao

    companion object {
        private var dbInstance: AppDatabase? = null

        fun getAppDb(context: Context): AppDatabase {
            if (dbInstance == null){
                dbInstance = Room.databaseBuilder<AppDatabase>(
                    context.applicationContext, AppDatabase::class.java,
                    "DataDB"
                ).allowMainThreadQueries()
                    .build()
            }

            return dbInstance!!
        }
    }

}