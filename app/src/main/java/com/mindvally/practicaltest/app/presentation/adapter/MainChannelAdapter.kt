package com.mindvally.practicaltest.app.presentation.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.mindvally.practicaltest.R
import com.mindvally.practicaltest.apiclient.apisupport.response.channel.Channel


class MainChannelAdapter(
    private val context: Context,
    private val channel: List<Channel>
) : RecyclerView.Adapter<MainChannelAdapter.MainAdapterViewHolder>() {

    lateinit var courseAdapter: CourseAdapter
    lateinit var seriesAdapter: SeriesAdapter
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainAdapterViewHolder {
        return MainAdapterViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.channel_recycle_view, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MainAdapterViewHolder, position: Int) = holder.run {
        onBind(position)
    }

    override fun getItemCount(): Int {
        return if (channel.isEmpty()) {
            0
        } else {
            channel.size
        }
    }

    inner class MainAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val cIcon = itemView.findViewById<ImageView>(R.id.cIcon)
        private val cTitle = itemView.findViewById<TextView>(R.id.cTitle)
        private val episodes = itemView.findViewById<TextView>(R.id.episodes)
        private var seriesRecycleView = itemView.findViewById<RecyclerView>(R.id.seriesRecycleView)
        private var courseRecycleView = itemView.findViewById<RecyclerView>(R.id.courseRecycleView)

        @SuppressLint("SetTextI18n")
        fun onBind(position: Int) {
            channel[position].iconAsset?.url?.let {
                Glide.with(context).load(it)
                    .circleCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.drawable.ic_baseline_error_24)
                    .into(cIcon)
            }

            if (channel[position].series.isNotEmpty()) {

                channel[position].series.let {
                    courseRecycleView.visibility = View.GONE
                    seriesRecycleView.visibility = View.VISIBLE
                    seriesAdapter = SeriesAdapter(it, context)
                    seriesRecycleView.layoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    seriesRecycleView.adapter = seriesAdapter
                }

            } else {
                channel[position].latestMedia.let {
                    courseRecycleView.visibility = View.VISIBLE
                    seriesRecycleView.visibility = View.GONE
                    courseAdapter = CourseAdapter(it, context)
                    courseRecycleView.layoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    courseRecycleView.adapter = courseAdapter
                }
            }


            cTitle.text = channel[position].title
            episodes.text = channel[position].mediaCount.toString() + " episodes"

        }

    }

}