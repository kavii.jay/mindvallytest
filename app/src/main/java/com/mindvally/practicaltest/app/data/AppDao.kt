package com.mindvally.practicaltest.app.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.mindvally.practicaltest.apiclient.apisupport.response.category.CategoryResponse
import com.mindvally.practicaltest.apiclient.apisupport.response.channel.ChannelResponse
import com.mindvally.practicaltest.apiclient.apisupport.response.newepisode.NewEpisodesResponse

import kotlinx.coroutines.flow.Flow

@Dao
interface AppDao {

    // episodes
    @Query("SELECT * FROM newEpisodes")
    fun getAllNewEpisodes(): Flow<List<NewEpisodesResponse>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNewEpisodes(item: NewEpisodesResponse)

    @Query("DELETE FROM newEpisodes")
    suspend fun deleteEpisodes()

    // channel
    @Query("SELECT * FROM channel")
    fun getAllChannels(): Flow<List<ChannelResponse>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertChannels(item: ChannelResponse)

    @Query("DELETE FROM channel")
    suspend fun deletechannels()

    // category
    @Query("SELECT * FROM category")
    fun getAllCategoty(): Flow<List<CategoryResponse>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCategory(item: CategoryResponse)

    @Query("DELETE FROM category")
    suspend fun deleteCategory()

}