package com.mindvally.practicaltest.app.domain.repository

import com.mindvally.practicaltest.apiclient.apis.ApiService
import com.mindvally.practicaltest.app.data.AppDao
import com.mindvally.practicaltest.core.utill.networkBoundResource
import kotlinx.coroutines.delay
import javax.inject.Inject

class CategoryRepository @Inject constructor(
    private val apiService: ApiService,
    private val appDao: AppDao
) {
    fun getCategory() = networkBoundResource(

        query = {
            appDao.getAllCategoty()
        },
        fetch = {
            delay(5000)
            apiService.getCategory()

        },
        saveFetchResult = {
            appDao.deleteCategory()
            appDao.insertCategory(it.body()!!)
        }

    )
}