package com.mindvally.practicaltest.app.domain.repository


import com.mindvally.practicaltest.apiclient.apis.ApiService
import com.mindvally.practicaltest.app.data.AppDao
import com.mindvally.practicaltest.core.utill.networkBoundResource
import kotlinx.coroutines.*
import javax.inject.Inject

class NewEpisodeRepository @Inject constructor(
    private val apiService: ApiService,
    private val appDao: AppDao
) {


    fun getNewEpisodes() = networkBoundResource(

        query = {
            appDao.getAllNewEpisodes()
        },
        fetch = {
            delay(5000)
            apiService.getEpisodes()

        },
        saveFetchResult = {
            appDao.deleteEpisodes()
            appDao.insertNewEpisodes(it.body()!!)
        }

    )

}