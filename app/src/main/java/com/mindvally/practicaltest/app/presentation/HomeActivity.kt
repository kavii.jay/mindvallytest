package com.mindvally.practicaltest.app.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.mindvally.practicaltest.R
import com.mindvally.practicaltest.app.domain.viewmodel.HomeViewModel
import com.mindvally.practicaltest.app.presentation.adapter.CategoryAdapter
import com.mindvally.practicaltest.app.presentation.adapter.MainChannelAdapter
import com.mindvally.practicaltest.app.presentation.adapter.NewEpisodeAdapter
import com.mindvally.practicaltest.databinding.ActivityHomeBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {
    lateinit var binding: ActivityHomeBinding
    lateinit var homeViewModel: HomeViewModel

    lateinit var episodeAdapter: NewEpisodeAdapter
    lateinit var mainChannelAdapter: MainChannelAdapter
    lateinit var categoryAdapter: CategoryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initViewModel()

        binding.swipeRefresh.setOnRefreshListener {
            initViewModel()
        }
    }

    fun initViewModel() {
        lifecycleScope.launch {
            elementInvisible()
            delay(4000)
            elementVisible()
        }
        homeViewModel = ViewModelProvider(this)[HomeViewModel::class.java]
        homeViewModel.liveDataEpisode.observe(this){
            binding.apply {
                newEpisodeInclude.newEpideodeRecycle.apply {
                    val epList = it.data?.getOrNull(0)?.data?.media
                    if (epList != null && epList.isNotEmpty()){
                        episodeAdapter = NewEpisodeAdapter(epList, this@HomeActivity)
                        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                        adapter = episodeAdapter
                        binding.swipeRefresh.isRefreshing = false
                    }

                }
            }
        }
        homeViewModel.liveChannelData.observe(this){
            binding.apply {
                channelRecycle.apply {
                    val channelList = it.data?.getOrNull(0)?.data?.channels
                    if (channelList != null && channelList.isNotEmpty()){
                        mainChannelAdapter = MainChannelAdapter(this@HomeActivity,channelList)
                        layoutManager = LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false)
                        adapter = mainChannelAdapter
                        binding.swipeRefresh.isRefreshing = false
                    }
                }
            }
        }

        homeViewModel.liveCategoryData.observe(this){
            binding.apply {
                categoryView.categoryRecycleView.apply {
                    val cateList = it.data?.getOrNull(0)?.data?.categories
                    if (cateList != null && cateList.isNotEmpty()){
                        categoryAdapter = CategoryAdapter(this@HomeActivity,cateList)
                        layoutManager = GridLayoutManager(context,2)
                        adapter = categoryAdapter
                        binding.swipeRefresh.isRefreshing = false
                    }
                }
            }
        }

    }

    fun elementInvisible() {
        binding.apply {
            shimmerLay.visibility = View.VISIBLE
            shimmerLay.startShimmer()
            newEpisodeInclude.newEpisode.visibility = View.INVISIBLE
            newEpisodeInclude.mentoring.visibility = View.INVISIBLE
            newEpisodeInclude.newEpideodeRecycle.visibility = View.INVISIBLE
            channelRecycle.visibility = View.INVISIBLE
            view.visibility = View.INVISIBLE
            categoryView.browseByC.visibility = View.INVISIBLE
            categoryView.categoryRecycleView.visibility = View.INVISIBLE
        }

    }
    fun elementVisible() {
        binding.apply {
            shimmerLay.visibility = View.GONE
            shimmerLay.stopShimmer()
            newEpisodeInclude.newEpisode.visibility = View.VISIBLE
            newEpisodeInclude.mentoring.visibility = View.VISIBLE
            newEpisodeInclude.newEpideodeRecycle.visibility = View.VISIBLE
            channelRecycle.visibility = View.VISIBLE
            view.visibility = View.VISIBLE
            categoryView.browseByC.visibility = View.VISIBLE
            categoryView.categoryRecycleView.visibility = View.VISIBLE
        }

    }
}