package com.mindvally.practicaltest.app.domain.viewmodel

import androidx.lifecycle.*
import com.mindvally.practicaltest.app.domain.repository.CategoryRepository
import com.mindvally.practicaltest.app.domain.repository.ChannelRepository
import com.mindvally.practicaltest.app.domain.repository.NewEpisodeRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val newEpisodeRepository: NewEpisodeRepository,
    private val channelRepository: ChannelRepository,
    private val categoryRepository: CategoryRepository
) :
    ViewModel() {

    val liveDataEpisode = newEpisodeRepository.getNewEpisodes().asLiveData()
    val liveChannelData = channelRepository.getChannels().asLiveData()
    val liveCategoryData = categoryRepository.getCategory().asLiveData()

}