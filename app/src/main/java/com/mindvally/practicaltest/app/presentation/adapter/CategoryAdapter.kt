package com.mindvally.practicaltest.app.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mindvally.practicaltest.R
import com.mindvally.practicaltest.apiclient.apisupport.response.category.Category

class CategoryAdapter(private val context: Context, private val category: List<Category>) :
    RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        return CategoryViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.category_recycle_view, parent, false)
        )
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) = holder.run {
        onBind(position)
    }

    override fun getItemCount(): Int {
        return if (category.isEmpty()) {
            0
        } else
            category.size
    }

    inner class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val title = itemView.findViewById<TextView>(R.id.cateTitle)
        fun onBind(position: Int) {
            title.text = category[position].name.toString()
        }
    }

}