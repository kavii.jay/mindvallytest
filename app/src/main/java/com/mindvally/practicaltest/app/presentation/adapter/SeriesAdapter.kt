package com.mindvally.practicaltest.app.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.mindvally.practicaltest.R
import com.mindvally.practicaltest.apiclient.apisupport.response.channel.Series

class SeriesAdapter
    (
    private val series: List<Series>,
    private val context: Context
) :
    RecyclerView.Adapter<SeriesAdapter.EpisodeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EpisodeViewHolder {
        return EpisodeViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.series_recycle_view, parent, false)
        )
    }

    override fun onBindViewHolder(holder: EpisodeViewHolder, position: Int) = holder.run {
        onBind(position)
        val animation = AnimationUtils.loadAnimation(holder.itemView.context, R.anim.left_to_rigth)
        holder.itemView.startAnimation(animation)
    }

    override fun getItemCount(): Int {

        return if (series.isEmpty()) {
            0
        } else if (series.size > 6) {
            6
        } else
            series.size
    }


    inner class EpisodeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val coverImage = itemView.findViewById<ImageView>(R.id.seriesImage)
        private val epiTitle = itemView.findViewById<TextView>(R.id.seriesTitle)
        fun onBind(position: Int) {
            Glide.with(context).load(series[position].coverAsset.url)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.placeholder)
                .into(coverImage)
            epiTitle.text = series[position].title

        }

    }
}