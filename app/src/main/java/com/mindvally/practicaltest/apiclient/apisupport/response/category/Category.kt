package com.mindvally.practicaltest.apiclient.apisupport.response.category


import com.google.gson.annotations.SerializedName

data class Category(
    val name: String
)