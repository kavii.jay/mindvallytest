package com.mindvally.practicaltest.apiclient.apisupport.response.channel


data class Channel(
    val coverAsset: CoverAsset,
    val iconAsset: IconAsset,
    val id: String,
    val latestMedia: List<LatestMedia>,
    val mediaCount: Int,
    val series: List<Series>,
    val slug: String,
    val title: String
)