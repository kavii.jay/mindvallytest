package com.mindvally.practicaltest.apiclient.apisupport.response.channel


import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.mindvally.practicaltest.core.general.ConverterData

@Entity(tableName = "channel")
@TypeConverters(ConverterData::class)
data class ChannelResponse(
    @PrimaryKey val `data`: Data
)