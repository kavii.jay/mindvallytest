package com.mindvally.practicaltest.apiclient.apisupport.response.channel

data class Series(
    val coverAsset: CoverAsset,
    val id: String,
    val title: String
)