package com.mindvally.practicaltest.apiclient.apisupport.response.channel


import com.mindvally.practicaltest.apiclient.apisupport.response.channel.CoverAsset

data class LatestMedia(
    val coverAsset: CoverAsset,
    val title: String,
    val type: String
)