package com.mindvally.practicaltest.apiclient.apis

import com.mindvally.practicaltest.apiclient.apisupport.response.category.CategoryResponse
import com.mindvally.practicaltest.apiclient.apisupport.response.channel.ChannelResponse
import com.mindvally.practicaltest.apiclient.apisupport.response.newepisode.NewEpisodesResponse
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("z5AExTtw")
    suspend fun getEpisodes(): Response<NewEpisodesResponse>

    @GET("Xt12uVhM")
    suspend fun getChannels(): Response<ChannelResponse>

    @GET("A0CgArX3")
    suspend fun getCategory(): Response<CategoryResponse>

}