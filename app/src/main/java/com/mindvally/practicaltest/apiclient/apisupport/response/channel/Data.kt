package com.mindvally.practicaltest.apiclient.apisupport.response.channel


import com.mindvally.practicaltest.apiclient.apisupport.response.channel.Channel

data class Data(
    val channels: List<Channel>
)