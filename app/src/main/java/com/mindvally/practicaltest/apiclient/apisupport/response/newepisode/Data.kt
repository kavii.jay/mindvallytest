package com.mindvally.practicaltest.apiclient.apisupport.response.newepisode


data class Data(
    val media: List<Media>
)