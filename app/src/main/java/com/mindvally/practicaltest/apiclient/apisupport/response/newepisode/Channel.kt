package com.mindvally.practicaltest.apiclient.apisupport.response.newepisode


import com.google.gson.annotations.SerializedName

data class Channel(
    val title: String
)