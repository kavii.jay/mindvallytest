package com.mindvally.practicaltest.apiclient.apisupport.response.category


import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.mindvally.practicaltest.core.general.ConverterData

@Entity(tableName = "category")
@TypeConverters(ConverterData::class)
data class CategoryResponse(
    @PrimaryKey val `data`: Data
)