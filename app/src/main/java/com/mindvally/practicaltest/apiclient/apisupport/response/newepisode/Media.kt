package com.mindvally.practicaltest.apiclient.apisupport.response.newepisode


import com.mindvally.practicaltest.apiclient.apisupport.response.newepisode.Channel
import com.mindvally.practicaltest.apiclient.apisupport.response.newepisode.CoverAsset

data class Media(
    val channel: Channel,
    val coverAsset: CoverAsset,
    val title: String,
    val type: String
)