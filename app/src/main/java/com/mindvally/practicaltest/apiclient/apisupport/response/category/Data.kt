package com.mindvally.practicaltest.apiclient.apisupport.response.category


import com.mindvally.practicaltest.apiclient.apisupport.response.category.Category

data class Data(
    val categories: List<Category>
)