package com.mindvally.practicaltest.apiclient.apisupport.response.channel


import com.google.gson.annotations.SerializedName

data class IconAsset(
    val thumbnailUrl: String,
    val url: String
)