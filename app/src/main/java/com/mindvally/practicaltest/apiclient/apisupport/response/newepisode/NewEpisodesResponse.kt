package com.mindvally.practicaltest.apiclient.apisupport.response.newepisode


import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.mindvally.practicaltest.core.general.ConverterData


@Entity(tableName = "newEpisodes")
@TypeConverters(ConverterData::class)
data class NewEpisodesResponse(
    @PrimaryKey val `data`: Data
)