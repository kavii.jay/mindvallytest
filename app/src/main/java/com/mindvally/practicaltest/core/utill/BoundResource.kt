package com.mindvally.practicaltest.core.utill

import kotlinx.coroutines.flow.*

inline fun <ResultType, RequestType> networkBoundResource(
    crossinline query: () -> Flow<ResultType>,
    crossinline fetch: suspend () -> RequestType,
    crossinline saveFetchResult: suspend (RequestType) -> Unit,
    crossinline shouldFetch: (ResultType) -> Boolean = { true }
) = flow {
    val data = query().first()

    val flow = if (shouldFetch(data)) {
        emit(NetworkStatus.Loading(data))

        try {
            saveFetchResult(fetch())
            query().map { NetworkStatus.Success(it) }
        } catch (throwable: Throwable) {
            query().map { NetworkStatus.Error(throwable, it) }
        }
    } else {
        query().map { NetworkStatus.Success(it) }
    }

    emitAll(flow)
}