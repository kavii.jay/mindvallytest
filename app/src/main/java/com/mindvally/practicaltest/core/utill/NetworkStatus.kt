package com.mindvally.practicaltest.core.utill

sealed class NetworkStatus<T>(
    val data: T? = null,
    val error: Throwable? = null
) {
    class Success<T>(data: T) : NetworkStatus<T>(data)
    class Loading<T>(data: T? = null) : NetworkStatus<T>(data)
    class Error<T>(throwable: Throwable, data: T? = null) : NetworkStatus<T>(data, throwable)
}