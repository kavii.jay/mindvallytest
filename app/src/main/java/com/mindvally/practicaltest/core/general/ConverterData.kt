package com.mindvally.practicaltest.core.general

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.mindvally.practicaltest.apiclient.apisupport.response.newepisode.Data

class ConverterData {
    // to convert new episodes
    @TypeConverter
    fun fromJson(json: String): Data? {
        return Gson().fromJson(json, Data::class.java)
    }

    @TypeConverter
    fun toJson(data: Data?): String {
        return Gson().toJson(data)
    }


    // to convert channel
    @TypeConverter
    fun fromChannelJson(json: String): com.mindvally.practicaltest.apiclient.apisupport.response.channel.Data {
        return Gson().fromJson(
            json,
            com.mindvally.practicaltest.apiclient.apisupport.response.channel.Data::class.java
        )
    }

    @TypeConverter
    fun toChannelJson(data: com.mindvally.practicaltest.apiclient.apisupport.response.channel.Data?): String {
        return Gson().toJson(data)
    }

    // to convert category
    @TypeConverter
    fun fromCategorylJson(json: String): com.mindvally.practicaltest.apiclient.apisupport.response.category.Data {
        return Gson().fromJson(
            json,
            com.mindvally.practicaltest.apiclient.apisupport.response.category.Data::class.java
        )
    }

    @TypeConverter
    fun toCategorylJson(data: com.mindvally.practicaltest.apiclient.apisupport.response.category.Data?): String {
        return Gson().toJson(data)
    }
}