package com.mindvally.practicaltest

import androidx.lifecycle.*
import com.mindvally.practicaltest.core.utill.NetworkStatus
import kotlinx.coroutines.flow.Flow
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

object LiveDataTestUtil {

    /**
     * Gets the value of a LiveData safely.
     */
    @Throws(InterruptedException::class)
    fun <T> getValue(liveData: Flow<NetworkStatus<List<T>>>): T? {
        var data: T? = null
        val latch = CountDownLatch(1)
        val observer = object : Observer<T> {

            override fun onChanged(value: T) {
                data = value
                latch.countDown()
                liveData.removeObserver(this)
            }
        }

        liveData.asLiveData().observeForever(observer)
        latch.await(10, TimeUnit.SECONDS)

        return data
    }

}

private fun <T, T1> LiveData<T>.observeForever(observer: Observer<T1>) {
    this.observeForever(observer)
}

private fun <T> Any.removeObserver(observer: Observer<T>) {
    this.removeObserver(observer)
}

/**
 * Observes a [LiveData] until the `block` is done executing.
 */
fun <T> LiveData<T>.observeForTesting(block: () -> Unit) {
    val observer = Observer<T> { }
    try {
        observeForever(observer)
        block()
    } finally {
        removeObserver(observer)
    }
}