package com.mindvally.practicaltest.home


import com.mindvally.practicaltest.apiclient.apis.ApiService
import com.mindvally.practicaltest.app.data.AppDao
import com.mindvally.practicaltest.app.domain.repository.NewEpisodeRepository
import com.mindvally.practicaltest.base.BaseTestClass
import com.mindvally.practicaltest.LiveDataTestUtil
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import org.junit.Assert.assertTrue
import org.junit.Test
import org.robolectric.annotation.Config
import javax.inject.Inject


@Config(application = HiltTestApplication::class)
@HiltAndroidTest
class NewEpisodeRepositoryTest : BaseTestClass() {

    @Inject
    lateinit var apiService: ApiService

    @Inject
    lateinit var appDao: AppDao

    @Test
    fun getNewEpisodes() {

        val repository = createRepository()

        val result = LiveDataTestUtil.getValue(
            repository.getNewEpisodes()
        )

        if (result != null) {
            assertTrue(result.data.media.isNotEmpty())
        }

    }

    private fun createRepository(): NewEpisodeRepository {

        return NewEpisodeRepository(
            apiService, appDao
        )

    }

}