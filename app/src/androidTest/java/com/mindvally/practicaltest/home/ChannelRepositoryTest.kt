package com.mindvally.practicaltest.home

import com.mindvally.practicaltest.LiveDataTestUtil
import com.mindvally.practicaltest.apiclient.apis.ApiService
import com.mindvally.practicaltest.app.data.AppDao
import com.mindvally.practicaltest.app.domain.repository.ChannelRepository
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import org.junit.Assert
import org.junit.Test
import org.robolectric.annotation.Config
import javax.inject.Inject

@Config(application = HiltTestApplication::class)
@HiltAndroidTest
class ChannelRepositoryTest {

    @Inject
    lateinit var apiService: ApiService

    @Inject
    lateinit var appDao: AppDao

    @Test
    fun getNewEpisodes() {

        val repository = createRepository()

        val result = LiveDataTestUtil.getValue(
            repository.getChannels()
        )

        if (result != null) {
            Assert.assertTrue(result.data.channels.isEmpty())
        }

    }

    private fun createRepository(): ChannelRepository {

        return ChannelRepository(
            apiService, appDao
        )

    }
}