package com.mindvally.practicaltest.base

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import dagger.hilt.android.testing.CustomTestApplication
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltTestApplication
import org.junit.Before
import org.junit.Rule

@CustomTestApplication(HiltTestApplication::class)
abstract class BaseTestClass {

    @get:Rule(order = 0)
    var hiltAndroidRule = HiltAndroidRule(this)

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun before() {

        hiltAndroidRule.inject()
    }

}